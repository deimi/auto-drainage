
# Install espeasy

https://www.letscontrolit.com/wiki/index.php?title=Flash_script_linux

1. Get ESP tool
https://github.com/igrr/esptool-ck/releases/

2. Get latest espeasy binary and unzip
https://github.com/letscontrolit/ESPEasy/releases

3. Copy esptool, espeasy binary and flash_firmware.sh to same directory

4. Restart ESP in download mode

5. Execute flash_firmware.sh and follow instructions


# Connect to device AP

After flashing the ESP will start with Wifi AP. The password is `configesp`


# Backup/Restore

## Configs

Configs can be backed up and restored with the corresponding feature in `Tools`.  

## Rules

The rules need to be copied the bare way via the file system browser in `Tools`.
