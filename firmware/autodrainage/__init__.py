#!/usr/bin/python3

"""Controll my auto drainage system"""

from .version import version
from .autodrainage import start

__all__ = ['version', 'start']
