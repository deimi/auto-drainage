#!/usr/bin/python3

"""Current firmware version"""


def version():
    """Returns current firmware version"""
    return '0.0.1'
