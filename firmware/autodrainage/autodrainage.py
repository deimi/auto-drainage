#!/usr/bin/python3

"""Main module for the autodrainage systen"""

from time import sleep
from machine import Signal     # pylint: disable=E0401
try:
    from machine import Pin     # pylint: disable=E0401
except ImportError:
    Pin = None


class AutoDrainage:
    """Main class for the autodrainage systen"""

    def __init__(self):
        self.pump = Signal(Pin(13, Pin.OUT), invert=False)

    def start(self):
        '''For starting the system'''
        while True:
            self.turn_pump_on()
            self.sleep_for_x_seconds(60)
            # self.sleep_for_x_seconds(10)
            self.turn_pump_off()
            # self.sleep_for_x_seconds(86400)     # 24 hours
            self.sleep_for_x_seconds(43200)     # 12 hours
            # self.sleep_for_x_seconds(10)

    def turn_pump_on(self):
        '''Turns the pump on'''
        print('Turn pump on')
        self.pump.on()

    def turn_pump_off(self):
        '''Turns the pump off'''
        print('Turn pump off')
        self.pump.off()

    @staticmethod
    def sleep_for_x_seconds(seconds):
        '''Sleep for x seconds with printing countdown every 10 seconds'''
        count = 0
        for count in range(seconds, 0, -10):
            print(count, ' seconds till next action')
            sleep(10)


def start():
    """Factory"""
    my_autodrainage = AutoDrainage()
    my_autodrainage.start()
