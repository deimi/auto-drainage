'''This file is executed on every boot (including wake-boot from deepsleep)'''

from time import sleep

# import esp
# esp.osdebug(None)
# import uos
# import machine
# uos.dupterm(None, 1) # disable REPL on UART(0)
import gc
import webrepl  # pylint: disable=E0401
import network  # pylint: disable=E0401
import autodrainage

# Wait some time before we start anything, in case the
# self recurring hardware reset occurres
sleep(5)

webrepl.start()
gc.collect()

ap_if = network.WLAN(network.AP_IF)
ap_if.active(False)

autodrainage.start()
