# Development

## Local test

### Install requirements

```sh
snap install micropython
cd firmware
micropython -m upip install -r requirements.txt
```

### Execute tests

```sh
cd firmware
micropython -c "import unittest; unittest.main('test')"
```
or
```sh
cd firmware
./script/run_all_tests.sh
```