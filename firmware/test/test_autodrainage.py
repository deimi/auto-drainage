#!/usr/bin/python3

"""Unit tests for the auto drainage system"""

import unittest

import autodrainage

if __name__ == '__main__':
    unittest.main()


class TestAutodrainage(unittest.TestCase):
    """Test class"""

    def test_version(self):
        """Test version reading"""
        self.assertEqual(autodrainage.version(), '0.0.1')
